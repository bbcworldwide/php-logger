PHP Logger
==============================
This library is used to handle logging for PHP applications, and is comprised of two main components:
* JSONFormatter that formats the logs to send to Splunk.
* Correlation processor which adds a correlationId so that all linked requests can be grouped for log tracing and reporting


Composer
------------------------------
To include this in another project you need to add the following to the composer file.

In the `repositories` section add the following:
```
{
    "type": "vcs",
    "url": "https://bitbucket.org/bbcworldwide/php-logger.git"
}
```
And in the `require' section add the following:
```
"bbcworldwide/php-logger":     "{version}"
```

PSR-7 Request
------------------------------
If you are responding to a request to the service, you can pass in the `request` and the correlation processor will add
the correlationId and appId to the logs automatically.

`$logger = new LoggerFactory('Application Name', Logger::INFO, $request)`

Custom Implementation
------------------------------
If you require a custom implementation you can use the `BBCWorldwide\Logging\Processor\CorrelationProcessor`
and `BBCWorldwide\Logging\Formatter\JsonFormatter` to mannage the correlation Id and format for Splunk logs.
