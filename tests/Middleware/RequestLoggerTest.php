<?php

namespace BBCWorldwide\Logging\Tests;

use BBCWorldwide\Logging\Middleware\RequestLogger;
use Zend\Diactoros\Request;
use Psr\Log\LoggerInterface;
use Zend\Diactoros\Response;
use Zend\Diactoros\Uri;

class RequestLoggerTest extends \PHPUnit_Framework_TestCase
{
    public function testSuccess()
    {
        $request = new Request();
        $uriString = 'http://www.example.com/foo?bar=baz';
        $uri = new Uri($uriString);
        $request = $request
            ->withUri($uri)
            ->withMethod('GET')
            ->withHeader('content-type', 'application/xml');

        $response = new Response();

        $next = function ($request, $response) {
            return $response
                ->withStatus(200)
                ->withHeader('content-type', 'application/json');
        };

        $message = null;

        $logger = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $logger->expects($this->once())
            ->method('info')
            ->withAnyParameters()
            ->willReturnCallback(function ($message, $context) use ($uriString) {
                self::assertEquals('OK', $message);
                self::assertEquals('inbound-service-call', $context['type']);
                self::assertGreaterThan('0', $context['duration']);
                // Request
                self::assertEquals('GET', $context['request']['method']);
                self::assertEquals($uriString, $context['request']['uri']);
                // Response
                self::assertEquals(200, $context['response']['status']);
            });

        $middleware = new RequestLogger($logger);
        $middleware($request, $response, $next);
    }

    public function testClientError()
    {
        $request = new Request();
        $response = new Response();

        $next = function ($request, $response) {
            return $response->withStatus(401);
        };

        $message = null;

        $logger = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $logger->expects($this->once())
            ->method('warning')
            ->withAnyParameters()
            ->willReturnCallback(function ($message, $context) {
                self::assertEquals('Unauthorized', $message);
                self::assertEquals(401, $context['response']['status']);
            });

        $middleware = new RequestLogger($logger);
        $middleware($request, $response, $next);
    }

    public function testServerError()
    {
        $request = new Request();
        $response = new Response();

        $next = function ($request, $response) {
            return $response->withStatus(503);
        };

        $message = null;

        $logger = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $logger->expects($this->once())
            ->method('critical')
            ->withAnyParameters()
            ->willReturnCallback(function ($message, $context) {
                self::assertEquals('Service Unavailable', $message);
                self::assertEquals(503, $context['response']['status']);
            });

        $middleware = new RequestLogger($logger);
        $middleware($request, $response, $next);
    }
}
