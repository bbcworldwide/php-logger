<?php

namespace BBCWorldwide\Logging\Tests;

use BBCWorldwide\Logging\Formatter\JsonFormatter;
use BBCWorldwide\Logging\LoggerFactory;
use Monolog\Handler\StreamHandler;
use Zend\Diactoros\Request;
use Monolog\Logger;
use BBCWorldwide\Logging\Processor\CorrelationProcessor;

class LoggerFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testGetInstance()
    {
        $request = new Request();
        $instance = LoggerFactory::getInstance('example', 'info', $request);

        self::assertInstanceOf(Logger::class, $instance);
        self::assertInstanceOf(StreamHandler::class, $instance->getHandlers()[0]);
        self::assertInstanceOf(CorrelationProcessor::class, $instance->getProcessors()[0]);
        self::assertInstanceOf(JsonFormatter::class, $instance->getHandlers()[0]->getFormatter());
    }
}
