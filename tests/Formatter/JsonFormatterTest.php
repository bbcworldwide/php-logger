<?php

namespace BBCWorldwide\Logging\Tests;

use BBCWorldwide\Logging\Formatter\JsonFormatter;

class JsonFormatterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var JsonFormatter
     */
    protected $formatter;

    public function setUp()
    {
        $this->formatter = new JsonFormatter();
    }

    public function testBasicRecord()
    {
        $record =[
            'channel'       => 'example-channel',
            'message'       => 'example-message',
            'level_name'    => 'INFO',
            'datetime'      => new \DateTime(),
            'context'       => [],
            'extra'         => [],
        ];
        $formatted = json_decode($this->formatter->format($record), true);

        self::assertEquals($record['channel'], $formatted['name']);
        self::assertEquals($record['message'], $formatted['msg']);
        self::assertEquals('application-log', $formatted['type']);
        self::assertEquals($record['datetime']->format(\DateTime::ISO8601), $formatted['time']);
    }

    public function testContext()
    {
        $record =[
            'channel'       => 'example-channel',
            'message'       => 'example-message',
            'level_name'    => 'INFO',
            'datetime'      => new \DateTime(),
            'context'       => [
                'foo'   => 'bar',
                'type'  => 'custom-type'
            ],
            'extra'         => [],
        ];
        $formatted = json_decode($this->formatter->format($record), true);

        self::assertEquals($record['context']['foo'], $formatted['foo']);
        self::assertEquals($record['context']['type'], $formatted['type']);
    }

    public function testExtra()
    {
        $record =[
            'channel'       => 'example-channel',
            'message'       => 'example-message',
            'level_name'    => 'INFO',
            'datetime'      => new \DateTime(),
            'context'       => [],
            'extra'       => [
                'foo'   => 'bar',
            ],
        ];
        $formatted = json_decode($this->formatter->format($record), true);

        self::assertEquals($record['extra']['foo'], $formatted['foo']);
    }

    /**
     * @dataProvider levelProvider
     */
    public function testLevelMapping($source, $dest)
    {
        $record =[
            'channel'       => 'example-channel',
            'message'       => 'example-message',
            'level_name'    => $source,
            'datetime'      => new \DateTime(),
            'context'       => [],
            'extra'         => [],
        ];
        $formatted = json_decode($this->formatter->format($record), true);
        self::assertEquals($dest, $formatted['severity']);
    }

    public function levelProvider()
    {
        return [
            ['TRACE', 'trace'],
            ['DEBUG', 'debug'],
            ['INFO', 'info'],
            ['NOTICE', 'info'],
            ['WARNING', 'warn'],
            ['ERROR', 'error'],
            ['CRITICAL', 'fatal'],
            ['ALERT', 'fatal'],
            ['EMERGENCY', 'fatal'],
        ];
    }
}
