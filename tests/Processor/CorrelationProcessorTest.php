<?php
namespace BBCWorldwide\Logging\Tests;

use BBCWorldwide\Logging\Processor\CorrelationProcessor;
use Zend\Diactoros\Request;

class CorrelationProcessorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function useApiIdFromRequestHeader()
    {
        $appId = '1234';
        $request = new Request();
        $request = $request->withAddedHeader('x-application-id', $appId);
        $processor = new CorrelationProcessor($request);

        $record = ['message' => 'Test'];
        $processed = $processor($record);

        self::assertEquals($appId, $processed['extra']['appId']);
    }

    /**
     * @test
     */
    public function useCorrelationIdFromRequestHeader()
    {
        $correlationId = uniqid('', true);
        $request = new Request();
        $request = $request->withAddedHeader('x-request-correlation-id', $correlationId);
        $processor = new CorrelationProcessor($request);

        $record = ['message' => 'Test'];
        $processed = $processor($record);

        self::assertEquals($correlationId, $processed['extra']['correlationId']);
    }

    /**
     * @test
     */
    public function correlationIdIsGeneratedWhenNoHeader()
    {
        $request = new Request();
        $processor = new CorrelationProcessor($request);

        $record = ['message' => 'Test'];
        $processed = $processor($record);

        self::assertNotNull($processed['extra']['correlationId']);
    }

    /**
     * @test
     */
    public function setCorrelationIdToAddToRecord()
    {
        $correlationId = uniqid('', true);
        $processor = new CorrelationProcessor();
        $processor->setCorrelationId($correlationId);

        $record = ['message' => 'Test'];
        $processed = $processor($record);

        self::assertEquals($correlationId, $processed['extra']['correlationId']);
    }

    /**
     * @test
     */
    public function getCorrelationId()
    {
        $correlationId = uniqid('', true);
        $processor = new CorrelationProcessor();
        $processor->setCorrelationId($correlationId);

        self::assertEquals($correlationId, $processor->getCorrelationId());
    }

    /**
     * @test
     */
    public function correlationIdIsGeneratedWithoutRequest()
    {
        $processor = new CorrelationProcessor();
        self::assertNotNull($processor->getCorrelationId());
    }

    /**
     * @test
     */
    public function setAppIdToAddToRecord()
    {
        $appId = '1234';
        $processor = new CorrelationProcessor();
        $processor->setAppId($appId);

        $record = ['message' => 'Test'];
        $processed = $processor($record);

        self::assertEquals($appId, $processed['extra']['appId']);
    }

    /**
     * @test
     */
    public function getAppId()
    {
        $appId = '1234';
        $processor = new CorrelationProcessor();
        $processor->setAppId($appId);

        self::assertEquals($appId, $processor->getAppId());
    }

    /**
     * @test
     */
    public function appIdIsNull()
    {
        $processor = new CorrelationProcessor();
        self::assertNull($processor->getAppId());
    }

    /**
     * @test
     */
    public function useGeneratedCorrelationIdWhenNotSet()
    {
        $request = new Request();
        $processor = new CorrelationProcessor($request);

        $record = ['message' => 'Test'];
        $processed = $processor($record);

        self::assertNotEmpty($processed['extra']['correlationId']);
    }
}
