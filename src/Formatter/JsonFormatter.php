<?php

namespace BBCWorldwide\Logging\Formatter;

/**
 * Format logs in custom JSON schema.
 */
class JsonFormatter extends \Monolog\Formatter\JsonFormatter
{
    /**
     * @var array Internal log level to severity map.
     */
    protected $levelMap = [
        'TRACE' => 'trace',
        'DEBUG' => 'debug',
        'INFO' => 'info',
        'NOTICE' => 'info',
        'WARNING' => 'warn',
        'ERROR' => 'error',
        'CRITICAL' => 'fatal',
        'ALERT' => 'fatal',
        'EMERGENCY' => 'fatal',
    ];

    /**
     * {@inheritdoc}
     */
    public function format(array $record)
    {
        $formatterRecord = [
            'name' => $record['channel'],
            'type' => 'application-log',
            'msg' => $record['message'],
            'severity' => $this->levelMap[$record['level_name']],
            'time' => $record['datetime']->format(\DateTime::ISO8601),
        ];
        if (isset($record['context'])) {
            $formatterRecord = array_merge($formatterRecord, $record['context']);
        }
        if (isset($record['extra'])) {
            $formatterRecord = array_merge($formatterRecord, $record['extra']);
        }

        return parent::format($formatterRecord);
    }
}
