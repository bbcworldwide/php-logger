<?php

namespace BBCWorldwide\Logging\Processor;

use Psr\Http\Message\RequestInterface;

/**
 * Adds Unique IDs to correlate log messages.
 */
class CorrelationProcessor
{

    /**
     * @var string
     */
    protected $correlationId;

    /**
     * @var string
     */
    protected $appId;

    /**
     * Public constructor.
     *
     * @param RequestInterface $request
     */
    public function __construct(RequestInterface $request = null)
    {
        if ($request) {
            $appId = $request->getHeader('x-application-id');
            if (!empty($appId)) {
                $this->appId = $appId[0];
            }
            $correlationId = $request->getHeader('x-request-correlation-id');
            if (!empty($correlationId)) {
                $this->correlationId = $correlationId[0];
            }
        }

        // If the correlation id has not been set create one now.
        if ($this->correlationId === null) {
            $this->correlationId = uniqid('', true);
        }
    }

    /**
     * Add the application id and correlation id to the log record.  If a correlation id has not been set, one will
     * be automatically created.
     *
     * @param array $record
     * @return array
     */
    public function __invoke(array $record)
    {
        if (!empty($this->appId)) {
            $record['extra']['appId'] = $this->appId;
        }
        $record['extra']['correlationId'] = $this->correlationId;
        return $record;
    }

    /**
     * Get the correlation id to be used.
     *
     * @return string|null
     */
    public function getCorrelationId()
    {
        return $this->correlationId;
    }

    /**
     * Set the correlation id to track the service call across all requests.
     *
     * @param string $correlationId
     */
    public function setCorrelationId($correlationId)
    {
        $this->correlationId = $correlationId;
        return $this;
    }

    /**
     * Get the application id of the calling application.
     *
     * @return string|null
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * Set the application id of the application making the request.
     *
     * @param string $appId
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
        return $this;
    }
}
